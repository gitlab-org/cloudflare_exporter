package main

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	zonesActive                   prometheus.Gauge
	httpCountryRequests           *TimestampedMetricVec
	httpCountryBytes              *TimestampedMetricVec
	httpProtocolRequests          *TimestampedMetricVec
	httpResponses                 *TimestampedMetricVec
	httpTLSRequests               *TimestampedMetricVec
	httpColoRequests              *TimestampedMetricVec
	httpColoBytes                 *TimestampedMetricVec
	httpCachedRequests            *TimestampedMetricVec
	httpCachedBytes               *TimestampedMetricVec
	firewallEvents                *TimestampedMetricVec
	reputation                    *TimestampedMetricVec
	healthCheckEvents             *TimestampedMetricVec
	networkErrorLogs              *TimestampedMetricVec
	cfScrapes                     prometheus.Counter
	cfScrapeErrs                  prometheus.Counter
	cfLastSuccessTimestampSeconds prometheus.Gauge
)

func registerMetrics(reg prometheus.Registerer) {
	// zone metrics
	zonesActive = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "active",
			Help:      "Number of active zones in the target Cloudflare account",
		},
	)
	httpCountryRequests = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "http_country_requests_total",
			Help:      "Number of HTTP requests by country.",
		},
		[]string{"zone", "client_country_name"},
	)
	httpColoBytes = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "http_colo_bytes_total",
			Help:      "Number of HTTP bytes by Cloudflare colocation.",
		},
		[]string{"zone", "colocation"},
	)
	httpColoRequests = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "http_colo_requests_total",
			Help:      "Number of HTTP requests by Cloudflare colocation.",
		},
		[]string{"zone", "colocation"},
	)
	httpCountryBytes = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "http_country_bytes_total",
			Help:      "Number of HTTP bytes by country.",
		},
		[]string{"zone", "client_country_name"},
	)
	httpProtocolRequests = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "http_protocol_requests_total",
			Help:      "Number of HTTP requests by protocol.",
		},
		[]string{"zone", "client_http_protocol"},
	)
	httpTLSRequests = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "http_tls_requests_total",
			Help:      "Number of HTTP requests by TLS version.",
		},
		[]string{"zone", "client_tls_protocol"},
	)
	httpResponses = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "http_responses_total",
			Help:      "Number of HTTP responses by status.",
		},
		[]string{"zone", "edge_response_status", "origin_response_status"},
	)
	httpCachedRequests = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "http_cached_requests_total",
			Help:      "Number of cached HTTP requests served.",
		},
		[]string{"zone", "cacheStatus"},
	)
	httpCachedBytes = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "http_cached_bytes_total",
			Help:      "Number of cached HTTP bytes served.",
		},
		[]string{"zone", "cacheStatus"},
	)
	firewallEvents = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "firewall_events_total",
			Help:      "Number of firewall events.",
		},
		[]string{"zone", "action", "source", "ruleID", "edgeResponseStatus", "originResponseStatus"},
	)
	reputation = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "ip_reputation_requests_total",
			Help:      "Requests grouped by country,  IP reputation and Cloudflare product.",
		},
		[]string{"zone", "source", "reputation", "client_country_name"},
	)
	healthCheckEvents = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "health_check_events_total",
			Help:      "Number of health check events.",
		},
		[]string{"zone", "failure_reason", "health_check_name", "health_status", "origin_response_status", "region", "scope"},
	)
	networkErrorLogs = NewTimestampedMetricVec(
		prometheus.CounterValue,
		prometheus.Opts{
			Namespace: namespace,
			Subsystem: "zones",
			Name:      "network_error_logs_total",
			Help:      "Number of network error log events.",
		},
		[]string{"zone", "client_country_name", "client_ip_version", "colocation", "protocol", "phase", "type"},
	)

	// graphql metrics
	cfScrapes = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: "graphql",
			Name:      "scrapes_total",
			Help:      "Number of times this exporter has scraped cloudflare",
		},
	)
	cfScrapeErrs = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: "graphql",
			Name:      "scrape_errors_total",
			Help:      "Number of times this exporter has failed to scrape cloudflare",
		},
	)
	cfLastSuccessTimestampSeconds = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: namespace,
			Subsystem: "graphql",
			Name:      "last_success_timestamp_seconds",
			Help:      "Time that the analytics data was last updated.",
		},
	)

	if reg == nil {
		reg = prometheus.DefaultRegisterer
	}
	reg.MustRegister(zonesActive)
	reg.MustRegister(httpCountryRequests)
	reg.MustRegister(httpCountryBytes)
	reg.MustRegister(httpProtocolRequests)
	reg.MustRegister(httpTLSRequests)
	reg.MustRegister(httpColoBytes)
	reg.MustRegister(httpColoRequests)
	reg.MustRegister(httpResponses)
	reg.MustRegister(httpCachedRequests)
	reg.MustRegister(httpCachedBytes)
	reg.MustRegister(firewallEvents)
	reg.MustRegister(reputation)
	reg.MustRegister(healthCheckEvents)
	reg.MustRegister(networkErrorLogs)
	reg.MustRegister(cfScrapes)
	reg.MustRegister(cfScrapeErrs)
	reg.MustRegister(cfLastSuccessTimestampSeconds)
}
