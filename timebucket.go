package main

import "time"

type TimeBucket struct {
	endTime time.Time
	size    time.Duration
	delay   time.Duration
}

func newTimeBucket(endTime time.Time, bucketSize time.Duration, minDelay time.Duration) *TimeBucket {
	// Ensure the endTime is at least minDelay in the past.
	if endTime.After(time.Now().Add(-minDelay)) {
		endTime = time.Now().Add(-minDelay)
	}

	// Truncate to the size of the bucket
	endTime = endTime.Round(bucketSize)
	return &TimeBucket{
		endTime: endTime,
		size:    bucketSize,
		delay:   minDelay,
	}
}

func (b *TimeBucket) getStartTime() time.Time {
	return b.getEndTime().Add(-b.size)
}

func (b *TimeBucket) getEndTime() time.Time {
	return b.endTime
}

func (b *TimeBucket) untick() {
	b.endTime = b.endTime.Add(-b.size)
}

func (b *TimeBucket) tick() bool {
	var newEndTime = b.endTime.Add(b.size)
	var nowWithAdjustedDelay = time.Now().Add(-b.delay).Round(b.size)

	// Only tick if we can still keep our delay!
	if newEndTime.Before(nowWithAdjustedDelay) || newEndTime.Equal(nowWithAdjustedDelay) {
		b.endTime = newEndTime
		return true
	} else {
		return false
	}
}
