package main

import "github.com/machinebox/graphql"

var (
	graphQL = graphql.NewRequest(`
query ($zone_in: [String]!, $start_time: Time!, $end_time: Time!, $limit: Int!) {
  viewer {
    zones(filter: {zoneTag_in: $zone_in}) {
      zoneTag
      trafficCached: httpRequestsAdaptiveGroups(limit: $limit, filter: {datetime_gt: $start_time, datetime_lt: $end_time}, orderBy: [cacheStatus_ASC]) {
        count
        sum {
          edgeResponseBytes
        }
        dimensions {
          cacheStatus
          sampleInterval
        }
      }
      trafficCountry: httpRequestsAdaptiveGroups(limit: $limit, filter: {datetime_gt: $start_time, datetime_lt: $end_time}, orderBy: [clientCountryName_ASC]) {
        count
        sum {
          edgeResponseBytes
        }
        dimensions {
          clientCountryName
          sampleInterval
        }
      }
      trafficColo: httpRequestsAdaptiveGroups(limit: $limit, filter: {datetime_gt: $start_time, datetime_lt: $end_time}, orderBy: [coloCode_ASC]) {
        count
        sum {
          edgeResponseBytes
        }
        dimensions {
          coloCode
          sampleInterval
        }
      }
      trafficDetails: httpRequestsAdaptiveGroups(limit: $limit, filter: {datetime_gt: $start_time, datetime_lt: $end_time}, orderBy: [clientSSLProtocol_ASC]) {
        count
        sum {
          edgeResponseBytes
        }
        dimensions {
          clientSSLProtocol
          edgeResponseStatus
          originResponseStatus
          sampleInterval
          clientRequestHTTPProtocol
        }
      }
      networkErrorLogs: nelReportsAdaptiveGroups(limit: $limit, filter: {datetime_gt: $start_time, datetime_lt: $end_time}, orderBy: [clientIPCountryCode_ASC]){
        count
        dimensions {
          clientIPCountryCode
          clientIPVersion
          lastKnownGoodColoCode
          protocol
          phase
          type
        }
      }
      reputation: firewallEventsAdaptiveGroups(limit: $limit, filter: {datetime_gt: $start_time, datetime_lt: $end_time}, orderBy: [clientIPClass_ASC]) {
        count
        dimensions {
          source
          sampleInterval
          clientIPClass
          clientCountryName
        }
      }
      firewallEventsAdaptiveGroups(limit: $limit, filter: {datetime_gt: $start_time, datetime_lt: $end_time}, orderBy: [ruleId_ASC]) {
        count
        dimensions {
          action
          edgeResponseStatus
          originResponseStatus
          ruleId
          source
          sampleInterval
        }
      }
      healthCheckEventsGroups(limit: $limit, filter: {datetime_gt: $start_time, datetime_lt: $end_time}, orderBy: [healthCheckName_ASC]) {
        count
        dimensions {
          failureReason
          healthCheckName
          healthStatus
          region
          scope
          originResponseStatus
        }
      }
    }
  }
}
	`)
)
